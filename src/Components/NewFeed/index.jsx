import { useState } from 'react';
import { Form, Button, Col } from 'react-bootstrap';

function NewFeed(props) {

	const [author, setAuthor] = useState('')
	const [feedback, setFeedback] = useState('')

	return (
		<Form>
			<Form.Group controlId="formAuthor">
				<Form.Label>Автор</Form.Label>
				<Col xs={9}>
					<Form.Control
						onChange={(e) => setAuthor(e.target.value)}
						type="text" placeholder="Автор" />
				</Col>
			</Form.Group>

			<Form.Group controlId="formFeedback">
				<Form.Label>Отзыв</Form.Label>
				<Col xs={9}>
					<Form.Control onChange={(e) => setFeedback(e.target.value)}
						type="text" placeholder="Текст отзыва" as="textarea" rows={3} />
				</Col>
			</Form.Group>
			<br></br>
			<Button type="reset" variant="success" onClick={() => props.createNewFeed(author, feedback)}> Отправить отзыв
			</Button>
		</Form>
	);
}

export default NewFeed;