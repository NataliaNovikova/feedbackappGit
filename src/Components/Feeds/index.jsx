import { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';


function Feeds(props) {

	const [columns, setColumns] = useState([
		{ name: 'Дата', selector: 'date', sortable: true },
		{ name: 'Имя автора', selector: 'author', sortable: true },
		{ name: 'Отзыв', selector: 'feedback' }
	]);

	const [feedText, setFeedText] = useState([]);

	useEffect(() => {

		fetch(`/api/feeds`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ id: props.idForFeeds })
		})
			.then(
				(response) => {
					if (!response.ok) {
						alert("Ошибка HTTP: " + response.status)
					}
					else {
						return response.json()
					}
				}
			).then(data => {
				setFeedText(data)
			})
			.catch(error => console.log("error", error));
	}, [props.idForFeeds]);

	return (

		<>
			<DataTable
				columns={columns}
				data={feedText}
				pagination
				paginationPerPage={25}
				paginationRowsPerPageOptions={[5, 10, 25, 50]}

			/>
		</>
	)
}

export default Feeds;