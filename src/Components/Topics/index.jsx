import { useState, useEffect } from 'react';

function Topics(props) {
  const [data, setData] = useState([]);
  useEffect(() => {

    let url = "/json/topics.json";
    fetch(url)
      .then(
        (response) => {
          if (!response.ok) {
            alert("Ошибка HTTP: " + response.status)
          }
          else {
            return response.json()
          }
        }
      ).then(data => {
        setData(data)
      })
      .catch(error => console.log("error", error));
  }, [])
  return (
    <>
      <ul className="list-group" >
        {data.map((elem) => (
          <li key={elem.id} className="list-group-item" onClick={() => props.selectTopic(elem.id)}>{elem.title}</li>
        ))}
      </ul>
    </>
  )
}

export default Topics;