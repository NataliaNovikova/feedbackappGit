import { useState } from 'react';
import NewFeed from './Components/NewFeed'
import Topics from './Components/Topics'
import Feeds from './Components/Feeds'
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container'
import { Row, Col } from "react-bootstrap";

function App() {

  const [topicID, setTopicID] = useState(undefined);


  function createFeed(author, feedback) {

    fetch(`/api/newFeed`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        author,
        feedback,
        topic_id: topicID
      })

    }).then(
      (response) => {
        if (!response.ok) {
          alert("Ошибка HTTP: " + response.status)
        }
        else {
          return response.text()
        }
      }
    )
      .then((data) => {
        if (data == 'err')
          alert("Отзыв не прошел модерацию")
        else if (JSON.parse(data)) {
          let tid = topicID
          setTopicID(undefined)
          setTopicID(tid)
          alert("Ваш отзыв успешно добавлен")
        }
        else
          alert("При добавлении отзыва произошла ошибка")
      })
      .catch((err) => console.log(err));
  }

  return (
    <>
      <Container fluid="true">
        <Row className="justify-content-md-center">
          <Col md="auto"> <h1>Project: FeedbackApp</h1></Col>
        </Row>
        <Row>
          <Col sm={5}>
            <Topics selectTopic={setTopicID} />
          </Col>
          <Col sm={7}>
            <Feeds idForFeeds={topicID} />
            <NewFeed createNewFeed={createFeed} />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default App;
